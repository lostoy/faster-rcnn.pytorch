#!/usr/bin/env bash
LOG="logs/voc_vgg_align//train.txt.`date +'%Y-%m-%d_%H-%M-%S'`"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"
CUDA_VISIBLE_DEVICES=0 python trainval_net.py --dataset pascal_voc --net vgg16 \
                       --bs 1 --nw 0 \
                       --lr 1e-3 --lr_decay_step 5 --epochs 7 \
                       --cuda --disp_interval 20